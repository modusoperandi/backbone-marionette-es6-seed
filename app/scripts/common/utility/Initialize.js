'use strict';

export default function (options) {

    var EnvConfig = null,

        LocConfig = {
            facebookId: options.local.facebookId,
            redirectUrl: options.local.redirectUrl
        },

        DevConfig = {
            facebookId: options.dev.facebookId,
            redirectUrl: options.dev.redirectUr
        },

        StageConfig = {
            facebookId: options.stage.facebookId,
            redirectUrl: options.stage.redirectUr
        },

        ClientStageConfig = {
            facebookId: options.clientStage.facebookId,
            redirectUrl: options.clientStage.redirectUr
        },

        ProdConfig = {
            facebookId: options.prod.facebookId,
            redirectUrl: options.prod.redirectUr
        };

    if (window.location.host === options.local.currentHost) {
        EnvConfig = LocConfig; // set to false to disable this validation
    } else if (window.location.host === 'dev.modopdev.com') {
        EnvConfig = DevConfig;
    } else if (window.location.host === 'stage.modopdev.com') {
        EnvConfig = StageConfig;
    } else {
        EnvConfig = ProdConfig;
    }
    window.fbAsyncInit = function () {
        FB.init({
            appId   : EnvConfig.facebookId,
            xfbml: true,
            version: 'v2.4'
        });
    };

}
