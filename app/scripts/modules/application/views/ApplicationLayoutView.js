'use strict';
import template from '../templates/applicationLayoutView.hbs';


export default Marionette.LayoutView.extend({
    get el() {
        return "[data-marionette-el='application']";
    },
    get template() {
        return template;
    },
    regions() {
        return {
            mainContent: "[data-marionette-region='main-content']"
        };
    }
});
