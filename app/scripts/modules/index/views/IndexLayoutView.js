'use strict';

import 'script!foundation-sites/js/foundation.tabs';
import 'script!foundation-sites/js/foundation.equalizer';
import template from 'modules/index/templates/IndexLayoutView.hbs';

export default Marionette.LayoutView.extend({
    get template() {
        return template;
    },
    onBeforeShow() {
        $('body').removeClass().addClass('index');
    },
    onAttach() {
        $('[data-tabs], [data-equalizer]').foundation();
    }
});
