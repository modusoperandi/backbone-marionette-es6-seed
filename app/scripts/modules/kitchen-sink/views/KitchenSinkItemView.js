'use strict';

import 'script!foundation-sites/dist/plugins/foundation.abide';
import 'script!foundation-sites/dist/plugins/foundation.accordion';
import 'script!foundation-sites/dist/plugins/foundation.accordionMenu';
import 'script!foundation-sites/dist/plugins/foundation.dropdown';
import 'script!foundation-sites/dist/plugins/foundation.drilldown';
import 'script!foundation-sites/dist/plugins/foundation.dropdownMenu';
import 'script!foundation-sites/dist/plugins/foundation.equalizer';
import 'script!foundation-sites/dist/plugins/foundation.interchange';
import 'script!foundation-sites/dist/plugins/foundation.magellan';
import 'script!foundation-sites/dist/plugins/foundation.offcanvas';
import 'script!foundation-sites/dist/plugins/foundation.orbit';
import 'script!foundation-sites/dist/plugins/foundation.reveal';
/// ---------------------------------------------------------------------------
// Slider Component Defunct the slider component is commented out for now
// due to this issue > https://github.com/zurb/foundation-sites/pull/9006
// import 'script!foundation-sites/dist/plugins/foundation.slider';
// Zurb's git claims fixed all come with the foundation 6.2.4 release
// which was due to on 7.25.16 but as of 8.12.16 we are still at 6.2.3
/// ---------------------------------------------------------------------------
import 'script!foundation-sites/dist/plugins/foundation.sticky';
import 'script!foundation-sites/dist/plugins/foundation.tabs';
import 'script!foundation-sites/dist/plugins/foundation.toggler';
import 'script!foundation-sites/dist/plugins/foundation.tooltip';

import template from 'modules/kitchen-sink/templates/KitchenSinkItemView.hbs';

export default Marionette.ItemView.extend({
    get template() {
        return template;
    },
    events() {
    },
    onBeforeRender() {
        $('body').removeClass().addClass('kitchen-sink');
    },
    onAttach() {
        var foundationInit = $('[data-accordion],[data-accordion-menu],[data-abide],[data-drilldown],[data-dropdown-menu],[data-dropdown],[data-equalizer],[data-interchange],[data-magellan],[data-off-canvas],[data-orbit],[data-reveal]/* ,[data-slider] */,[data-sticky],[data-tabs],[data-toggler],[data-tooltip]');
        foundationInit.foundation();
    }
});
