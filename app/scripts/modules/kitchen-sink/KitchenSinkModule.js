'use strict';

import KitchenSinkRouter from 'modules/kitchen-sink/KitchenSinkRouter';
import KitchenSinkController from 'modules/kitchen-sink/KitchenSinkController';

export default Marionette.Application.extend({
    initialize(options) {
        // autostart the module, comment this out to call it manually
        this.start(options);
    },
    start(options) {
        var kitchenSinkChannel = Radio.channel('kitchenSink');
        var kitchenSinkController = new KitchenSinkController({container: options.container});
        var kitchenSinkRouter = new KitchenSinkRouter({
            controller: kitchenSinkController
        });
    },
    stop() {
        this.controller.stop();
    }
});
