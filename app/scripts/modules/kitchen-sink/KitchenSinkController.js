'use strict';

import KitchenSinkCollection from 'modules/kitchen-sink/collections/KitchenSinkCollection';
import KitchenSinkItemView from 'modules/kitchen-sink/views/KitchenSinkItemView';

export default Marionette.Controller.extend({
    initialize(options) {
        this.options = options;
        this.collection = new KitchenSinkCollection();
    },
    kitchenSink() {
        this.kitchenSinkItemView = new KitchenSinkItemView();
        this.collection.fetch().then(function () {
            this.options.container.show(this.kitchenSinkItemView);
        }.bind(this));
    }
});
